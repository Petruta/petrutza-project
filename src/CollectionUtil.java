import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Petruta on 7/25/2016.
 */
public class CollectionUtil {

    public  static void printMatrix(int[][] matrix){
        for(int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[0].length; j++){
                System.out.print(" " +matrix[i][j]);


            }
            System.out.print("\n");
        }
    }

    public static char[] reverse(char[] letters){
        char[] reverse = new char[letters.length];
        int j =0 ;
        for (int i = letters.length-1; i >= 0 ; i--){
            reverse[j] = letters[i];
            j++;

        }
        return reverse;
    }

    public static List<Integer> reverseManually(List<Integer> numbers){
        List<Integer> reverse = new ArrayList<Integer>();
        for (int i = numbers.size()-1; i >= 0 ; i--){
            reverse.add(numbers.get(i));
        }
        return reverse;
    }

    public static List<Integer> reverseWithCollections(List<Integer> numbers){
        Collections.reverse(numbers);
        return numbers;
    }

    public  static void main(String[] args) {

        char[] letters = {'e', 'v', 'o', 'l', '4'};
        int matrix[][]={{50,60,55,67,70},{62,65,70,70,81},{72,66,77,80,69}};

        List<Integer> numbers = Arrays.asList(78, 90, 89,100);
        System.out.print(reverse(letters));
        System.out.print("\n");
        System.out.println(reverseManually(numbers));
        System.out.println(reverseWithCollections(numbers));
        System.out.println("Read matrix : ");
        printMatrix(matrix);
    }
}
