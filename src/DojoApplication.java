import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nagype on 8/2/2016.
 */
public class DojoApplication {

    public static   void main(String[] args) throws ParseException {
        DojoUtil dojoUtil = new DojoUtil();
        String date = "7-Aug-2016";
        System.out.println("\033[1mThe parameter weekday is true if it is a weekday, and the parameter vacation is true if we are on vacation." +
                "\n We sleep in if it is not a weekday or we're on vacation. Return true if we sleep in.\033[0m");
        System.out.println("We can sleep in " + date + " " + dojoUtil.weSleep(date));
        System.out.println("We can sleep  " + dojoUtil.weSleep2(true,false));
        System.out.println("We can sleep  " + dojoUtil.weSleep2(true,true));
        System.out.println("We can sleep  " + dojoUtil.weSleep2(false,false));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("We have two monkeys, a and b, and the parameters aSmile and bSmile indicate if each is smiling. " +
                "\n We are in trouble if they are both smiling or if neither of them is smiling. Return true if we are in trouble.");
        System.out.println("We are in trouble " + dojoUtil.monkeys(true, false));
        System.out.println("We are in trouble " + dojoUtil.monkeys(true, true));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given two int values, return their sum. Unless the two values are the same, then return double their sum.");
        System.out.println("Sum " + dojoUtil.sum(3,4));
        System.out.println("Sum " + dojoUtil.sum(3,3));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given an int n, return the absolute difference between n and 21, except return double the absolute difference if n is over 21.");
        System.out.println("Absolute " + dojoUtil.absolute(6));
        System.out.println("Absolute " + dojoUtil.absolute(25));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("We have a loud talking parrot. The \"hour\" parameter is the current hour time in the range 0..23." +
                " /n We are in trouble if the parrot is talking and the hour is before 7 or after 20. Return true if we are in trouble.");
        System.out.println("Parrot " + dojoUtil.hour(true, 23));
        System.out.println("Parrot " + dojoUtil.hour(false, 23));
        System.out.println("Parrot " + dojoUtil.hour(true, 10));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given 2 ints, a and b, return true if one if them is 10 or if their sum is 10.");
        System.out.println(dojoUtil.numbers(10,5));
        System.out.println(dojoUtil.numbers(5,5));
        System.out.println(dojoUtil.numbers(4,5));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string and a non-negative int n, we'll say that the front of the string is the first 3 chars, or whatever is there if the string is less than length 3. Return n copies of the front;\n" +
                "\n" +
                "frontTimes(\"Chocolate\", 2) → \"ChoCho\"\n" +
                "frontTimes(\"Chocolate\", 3) → \"ChoChoCho\"\n" +
                "frontTimes(\"Abc\", 3) → \"AbcAbcAbc\"");
        System.out.println(dojoUtil.frontTimes("Chocolate", 2));
        System.out.println(dojoUtil.frontTimes("Chocolate", 3));
        System.out.println(dojoUtil.frontTimes("Abc", 3));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, return a new string made of every other char starting with the first, so \"Hello\" yields \"Hlo\".\n" +
                "\n" +
                "stringBits(\"Hello\") → \"Hlo\"\n" +
                "stringBits(\"Hi\") → \"H\"\n" +
                "stringBits(\"Heeololeo\") → \"Hello\"");
        System.out.println(dojoUtil.stringBits("Hello"));
        System.out.println(dojoUtil.stringBits("Hi"));
        System.out.println(dojoUtil.stringBits("Heeololeo"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, return the count of the number of times that a substring length 2 appears in the string and also as the last 2 chars of the string, so \"hixxxhi\" yields 1 (we won't count the end substring).\n" +
                "\n" +
                "last2(\"hixxhi\") → 1\n" +
                "last2(\"xaxxaxaxx\") → 1\n" +
                "last2(\"axxxaaxx\") → 2");
        System.out.println(dojoUtil.last2("hixxhi"));
        System.out.println(dojoUtil.last2("xaxxaxaxx"));
        System.out.println(dojoUtil.last2("axxxaaxx"));
        System.out.println(dojoUtil.last2("h"));
        System.out.println(dojoUtil.last2(""));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given an array of ints, return the number of 9's in the array.\n" +
                "\n" +
                "arrayCount9([1, 2, 9]) → 1\n" +
                "arrayCount9([1, 9, 9]) → 2\n" +
                "arrayCount9([1, 9, 9, 3, 9]) → 3");
        int[] array1 = {1, 2, 9} ;
        int[] array2 = {1, 9, 9} ;
        int[] array3 = {1, 9, 9, 3, 9} ;
        System.out.println(dojoUtil.arrayCount9(array1));
        System.out.println(dojoUtil.arrayCount9(array2));
        System.out.println(dojoUtil.arrayCount9(array3));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given an array of ints, return true if one of the first 4 elements in the array is a 9. The array length may be less than 4.\n" +
                "\n" +
                "arrayFront9([1, 2, 9, 3, 4]) → true\n" +
                "arrayFront9([1, 2, 3, 4, 9]) → false\n" +
                "arrayFront9([1, 2, 3, 4, 5]) → false");
        int[] array4 = {1, 2, 9, 3, 4} ;
        int[] array5 = {1, 2, 3, 4, 9} ;
        int[] array6 = {1, 2, 3, 4, 5} ;
        System.out.println(dojoUtil.arrayFront9(array4));
        System.out.println(dojoUtil.arrayFront9(array5));
        System.out.println(dojoUtil.arrayFront9(array6));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, return a string made of the chars at indexes 0,1, 4,5, 8,9 ... so \"kittens\" yields \"kien\".\n" +
                "\n" +
                "altPairs(\"kitten\") → \"kien\"\n" +
                "altPairs(\"Chocolate\") → \"Chole\"\n" +
                "altPairs(\"CodingHorror\") → \"Congrr\"");
        System.out.println(dojoUtil.altPairs("Chocolate"));
        System.out.println(dojoUtil.altPairs("kitten"));
        System.out.println(dojoUtil.altPairs("CodingHorror"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given two strings, a and b, return the result of putting them together in the order abba, e.g. \"Hi\" and \"Bye\" returns \"HiByeByeHi\".\n" +
                "\n" +
                "makeAbba(\"Hi\", \"Bye\") → \"HiByeByeHi\"\n" +
                "makeAbba(\"Yo\", \"Alice\") → \"YoAliceAliceYo\"\n" +
                "makeAbba(\"What\", \"Up\") → \"WhatUpUpWhat\"");
        System.out.println(dojoUtil.makeAbba("Hi", "Bye"));
        System.out.println(dojoUtil.makeAbba("Yo", "Alice"));
        System.out.println(dojoUtil.makeAbba("What", "Up"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("The web is built with HTML strings like \"<i>Yay</i>\" which draws Yay as italic text. " +
                "In this example, the \"i\" tag makes <i> and </i> which surround the word \"Yay\". Given tag and word strings, create the HTML string with tags around the word, e.g. \"<i>Yay</i>\".\n" +
                "\n" +
                "makeTags(\"i\", \"Yay\") → \"<i>Yay</i>\"\n" +
                "makeTags(\"i\", \"Hello\") → \"<i>Hello</i>\"\n" +
                "makeTags(\"cite\", \"Yay\") → \"<cite>Yay</cite>\"\n");
        System.out.println(dojoUtil.makeTags("i", "Yay"));
        System.out.println(dojoUtil.makeTags("i", "Hello"));
        System.out.println(dojoUtil.makeTags("cite", "Yay"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given an \"out\" string length 4, such as \"<<>>\", and a word, return a new string where the word is in the middle of the out string, e.g. \"<<word>>\". Note: use str.substring(i, j) to extract the String starting at index i and going up to but not including index j.");
        System.out.println(dojoUtil.putInMiddle("<<>>", "word"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, return a new string made of 3 copies of the last 2 chars of the original string. The string length will be at least 2.\n" +
                "\n" +
                "extraEnd(\"Hello\") → \"lololo\"\n" +
                "extraEnd(\"ab\") → \"ababab\"\n" +
                "extraEnd(\"Hi\") → \"HiHiHi\"");
        System.out.println(dojoUtil.extraEnd("Hello"));
        System.out.println(dojoUtil.extraEnd("ab"));
        System.out.println(dojoUtil.extraEnd("Hi"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, take the last char and return a new string with the last char added at the front and back, so \"cat\" yields \"tcatt\". The original string will be length 1 or more.\n" +
                "\n" +
                "backAround(\"cat\") → \"tcatt\"\n" +
                "backAround(\"Hello\") → \"oHelloo\"\n" +
                "backAround(\"a\") → \"aaa\"");
        System.out.println(dojoUtil.backAround("cat"));
        System.out.println(dojoUtil.backAround("Hello"));
        System.out.println(dojoUtil.backAround("a"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, we'll say that the front is the first 3 chars of the string. If the string length is less than 3, the front is whatever is there. Return a new string which is 3 copies of the front.\n" +
                "front3(\"Java\") → \"JavJavJav\"\n" +
                "front3(\"Chocolate\") → \"ChoChoCho\"\n" +
                "front3(\"abc\") → \"abcabcabc\"");
        System.out.println(dojoUtil.front3("Java"));
        System.out.println(dojoUtil.front3("Chocolate"));
        System.out.println(dojoUtil.front3("abc"));
        System.out.println(dojoUtil.front3("ab"));
        System.out.println(dojoUtil.front3("a"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, return a new string where \"not \" has been added to the front. However, if the string already begins with \"not\", return the string unchanged. Note: use .equals() to compare 2 strings.");
        System.out.println(dojoUtil.notString("notddd"));
        System.out.println(dojoUtil.notString("test"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given 2 int values, return true if one is negative and one is positive. Except if the parameter \"negative\" is true, then return true only if both are negative.");
        System.out.println(dojoUtil.posNeg(3,-3,true));
        System.out.println(dojoUtil.posNeg(3,-3,false));
        System.out.println(dojoUtil.posNeg(3,3,true));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Return true if the given non-negative number is a multiple of 3 or a multiple of 5. Use the % \"mod\" operator -- see Introduction to Mod\n" +
                "\n" +
                "or35(3) → true\n" +
                "or35(10) → true\n" +
                "or35(8) → false");
        System.out.println(dojoUtil.or35(3));
        System.out.println(dojoUtil.or35(10));
        System.out.println(dojoUtil.or35(8));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, take the first 2 chars and return the string with the 2 chars added at both the front and back, so \"kitten\" yields\"kikittenki\". If the string length is less than 2, use whatever chars are there.\n" +
                "\n" +
                "front22(\"kitten\") → \"kikittenki\"\n" +
                "front22(\"Ha\") → \"HaHaHa\"\n" +
                "front22(\"abc\") → \"ababcab\"");
        System.out.println(dojoUtil.front22("kitten"));
        System.out.println(dojoUtil.front22("Ha"));
        System.out.println(dojoUtil.front22("abc"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, return true if the string starts with \"hi\" and false otherwise.\n" +
                "\n" +
                "startHi(\"hi there\") → true\n" +
                "startHi(\"hi\") → true\n" +
                "startHi(\"hello hi\") → false");
        System.out.println(dojoUtil.startHi("hi there"));
        System.out.println(dojoUtil.startHi("hi"));
        System.out.println(dojoUtil.startHi("hello hi"));
        System.out.println(dojoUtil.startHi("h"));
        System.out.println(dojoUtil.startHi(""));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given two temperatures, return true if one is less than 0 and the other is greater than 100.\n" +
                "\n" +
                "icyHot(120, -1) → true\n" +
                "icyHot(-1, 120) → true\n" +
                "icyHot(2, 120) → false");
        System.out.println(dojoUtil.icyHot(120, -1));
        System.out.println(dojoUtil.icyHot(-1, 120));
        System.out.println(dojoUtil.icyHot(2, 120));
        System.out.println(dojoUtil.icyHot(-2, -2));
        System.out.println(dojoUtil.icyHot(120, 120));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string and a non-negative int n, return a larger string that is n copies of the original string.\n" +
                "\n" +
                "stringTimes(\"Hi\", 2) → \"HiHi\"\n" +
                "stringTimes(\"Hi\", 3) → \"HiHiHi\"\n" +
                "stringTimes(\"Hi\", 1) → \"Hi\"");
        System.out.println(dojoUtil.stringTimes("Hi", 3));
        System.out.println(dojoUtil.stringTimes("Hi", 2));
        System.out.println(dojoUtil.stringTimes("Hi", 1));
        System.out.println(dojoUtil.stringTimes("Hi", 0));
        System.out.println(dojoUtil.stringTimes("code", 3));
        System.out.println(dojoUtil.stringTimes("x", 4));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given an array of ints length 3, return the sum of all the elements.\n" +
                "\n" +
                "sum3([1, 2, 3]) → 6\n" +
                "sum3([5, 11, 2]) → 18\n" +
                "sum3([7, 0, 0]) → 7");
        int[] array7 = {1,2,3};
        int[] array8 = {5, 11, 2};
        int[] array40 = {7,0,0};
        System.out.println(dojoUtil.sum3(array7));
        System.out.println(dojoUtil.sum3(array40));
        System.out.println(dojoUtil.sum3(array8));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given 2 arrays of ints, a and b, return true if they have the same first element or they have the same last element. Both arrays will be length 1 or more.\n" +
                "\n" +
                "commonEnd([1, 2, 3], [7, 3]) → true\n" +
                "commonEnd([1, 2, 3], [7, 3, 2]) → false\n" +
                "commonEnd([1, 2, 3], [1, 3]) → true");
        int[] array9={1,2,3};
        int[] array10 = {7,3};
        int[] array11 = {7,3,2};
        int[] array12 = {1,3};
        System.out.println(dojoUtil.commonEnd(array9, array10));
        System.out.println(dojoUtil.commonEnd(array9, array11));
        System.out.println(dojoUtil.commonEnd(array9, array12));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given an array of ints, return true if the array is length 1 or more, and the first element and the last element are equal.\n" +
                "\n" +
                "sameFirstLast([1, 2, 3]) → false\n" +
                "sameFirstLast([1, 2, 3, 1]) → true\n" +
                "sameFirstLast([1, 2, 1]) → true");
        int[] array13 = {1,2,3,1};
        int[] array14 = {1,2,1};
        System.out.println(dojoUtil.sameFirstLast(array9));
        System.out.println(dojoUtil.sameFirstLast(array13));
        System.out.println(dojoUtil.sameFirstLast(array14));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given an array of ints, return true if 6 appears as either the first or last element in the array. The array will be length 1 or more.\n" +
                "\n" +
                "firstLast6([1, 2, 6]) → true\n" +
                "firstLast6([6, 1, 2, 3]) → true\n" +
                "firstLast6([13, 6, 1, 2, 3]) → false");
        int[] array15 = {1,2,6};
        int[] array16 = {6,1,2,3};
        int[] array17 = {13,6,1,2,3};
        System.out.println(dojoUtil.firstLast6(array15));
        System.out.println(dojoUtil.firstLast6(array16));
        System.out.println(dojoUtil.firstLast6(array17));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given 2 strings, return their concatenation, except omit the first char of each. The strings will be at least length 1.\n" +
                "\n" +
                "nonStart(\"Hello\", \"There\") → \"ellohere\"\n" +
                "nonStart(\"java\", \"code\") → \"avaode\"\n" +
                "nonStart(\"shotl\", \"java\") → \"hotlava\"");
        System.out.println(dojoUtil.nonStart("hello", "there"));
        System.out.println(dojoUtil.nonStart("java", "code"));
        System.out.println(dojoUtil.nonStart("shotl", "java"));
        System.out.println(dojoUtil.nonStart("ho", "two"));
        System.out.println(dojoUtil.nonStart("ho", "kk"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string, return a version without the first and last char, so \"Hello\" yields \"ell\". The string length will be at least 2.\n" +
                "\n" +
                "withoutEnd(\"Hello\") → \"ell\"\n" +
                "withoutEnd(\"java\") → \"av\"\n" +
                "withoutEnd(\"coding\") → \"odin\"");
        System.out.println(dojoUtil.withoutEnd("hello"));
        System.out.println(dojoUtil.withoutEnd("java"));
        System.out.println(dojoUtil.withoutEnd("coding"));
        System.out.println("-----------------------------------------------------------------------------------------------");

        System.out.println("Given a string of even length, return the first half. So the string \"WooHoo\" yields \"Woo\".\n" +
                "\n" +
                "firstHalf(\"WooHoo\") → \"Woo\"\n" +
                "firstHalf(\"HelloThere\") → \"Hello\"\n" +
                "firstHalf(\"abcdef\") → \"abc\"");
        System.out.println(dojoUtil.firstHalf("woohoo"));
        System.out.println(dojoUtil.firstHalf("HelloThere"));
        System.out.println(dojoUtil.firstHalf("abcdef"));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given an array of ints length 3, return an array with the elements \"rotated left\" so {1, 2, 3} yields {2, 3, 1}.\n" +
                "\n" +
                "rotateLeft3([1, 2, 3]) → [2, 3, 1]\n" +
                "rotateLeft3([5, 11, 9]) → [11, 9, 5]\n" +
                "rotateLeft3([7, 0, 0]) → [0, 0, 7]");
        int[] array30 = {1, 2, 3};
        int[] array31 = {5, 11, 9};
        int[] array32 = {7, 0, 0};
        System.out.println(Arrays.toString(dojoUtil.rotateLeft3(array30)));
        System.out.println(Arrays.toString(dojoUtil.rotateLeft3(array31)));
        System.out.println(Arrays.toString(dojoUtil.rotateLeft3(array32)));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given an array of ints length 3, figure out which is larger between the first and last elements in the array, and set all the other elements to be that value. Return the changed array.\n" +
                "\n" +
                "maxEnd3([1, 2, 3]) → [3, 3, 3]\n" +
                "maxEnd3([11, 5, 9]) → [11, 11, 11]\n" +
                "maxEnd3([2, 11, 3]) → [3, 3, 3]");
        int[] array33 = {1, 2, 3};
        int[] array34 = {11, 5, 9};
        int[] array35 = {2, 11, 3};
        System.out.println(Arrays.toString(dojoUtil.maxEnd3(array33)));
        System.out.println(Arrays.toString(dojoUtil.maxEnd3(array34)));
        System.out.println(Arrays.toString(dojoUtil.maxEnd3(array35)));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("When squirrels get together for a party, they like to have cigars. A squirrel party is successful when the number of cigars is between 40 and 60, inclusive. Unless it is the weekend, in which case there is no upper bound on the number of cigars. Return true if the party with the given values is successful, or false otherwise.\n" +
                "\n" +
                "cigarParty(30, false) → false\n" +
                "cigarParty(50, false) → true\n" +
                "cigarParty(70, true) → true");
        System.out.println(dojoUtil.cigarParty(30, false));
        System.out.println(dojoUtil.cigarParty(50, false));
        System.out.println(dojoUtil.cigarParty(70, true));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("You and your date are trying to get a table at a restaurant. The parameter \"you\" is the stylishness of your clothes, in the range 0..10, and \"date\" is the stylishness of your date's clothes. The result getting the table is encoded as an int value with 0=no, 1=maybe, 2=yes. If either of you is very stylish, 8 or more, then the result is 2 (yes). With the exception that if either of you has style of 2 or less, then the result is 0 (no). Otherwise the result is 1 (maybe).\n" +
                "\n" +
                "dateFashion(5, 10) → 2\n" +
                "dateFashion(5, 2) → 0\n" +
                "dateFashion(5, 5) → 1");
        System.out.println(dojoUtil.dateFashion(5, 10));
        System.out.println(dojoUtil.dateFashion(5, 2));
        System.out.println(dojoUtil.dateFashion(5, 5));
        System.out.println(dojoUtil.dateFashion(10, 2));
        System.out.println(dojoUtil.dateFashion(2, 9));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given an array of ints, return the sum of the first 2 elements in the array. If the array length is less than 2, just sum up the elements that exist, returning 0 if the array is length 0.\n" +
                "\n" +
                "sum2([1, 2, 3]) → 3\n" +
                "sum2([1, 1]) → 2\n" +
                "sum2([1, 1, 1, 1]) → 2");
        int[] array36 = {1, 2, 3};
        int[] array37 = {1, 1};
        int[] array38 = {1, 1, 1, 1};
        System.out.println(dojoUtil.sum2(array36));
        System.out.println(dojoUtil.sum2(array37));
        System.out.println(dojoUtil.sum2(array38));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given an array of ints length 3, return a new array with the elements in reverse order, so {1, 2, 3} becomes {3, 2, 1}.\n" +
                "\n" +
                "reverse3([1, 2, 3]) → [3, 2, 1]\n" +
                "reverse3([5, 11, 9]) → [9, 11, 5]\n" +
                "reverse3([7, 0, 0]) → [0, 0, 7]");
        int[] array39 = {1, 2, 3};
        int[] array45 = {5, 11, 9};
        int[] array41 = {7, 0, 0};
        System.out.println(Arrays.toString(dojoUtil.reverse3(array39)));
        System.out.println(Arrays.toString(dojoUtil.reverse3(array45)));
        System.out.println(Arrays.toString(dojoUtil.reverse3(array41)));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("The squirrels in Palo Alto spend most of the day playing. In particular, they play if the temperature is between 60 and 90 (inclusive). Unless it is summer, then the upper limit is 100 instead of 90. Given an int temperature and a boolean isSummer, return true if the squirrels play and false otherwise.\n" +
                "\n" +
                "squirrelPlay(70, false) → true\n" +
                "squirrelPlay(95, false) → false\n" +
                "squirrelPlay(95, true) → true");
        System.out.println(dojoUtil.squirrelPlay(70, false));
        System.out.println(dojoUtil.squirrelPlay(95, false));
        System.out.println(dojoUtil.squirrelPlay(95, true));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given 2 ints, a and b, return their sum. However, sums in the range 10..19 inclusive, are forbidden, so in that case just return 20.\n" +
                "\n" +
                "sortaSum(3, 4) → 7\n" +
                "sortaSum(9, 4) → 20\n" +
                "sortaSum(10, 11) → 21");
        System.out.println(dojoUtil.sortaSum(3, 4));
        System.out.println(dojoUtil.sortaSum(9, 4));
        System.out.println(dojoUtil.sortaSum(10, 11));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given a number n, return true if n is in the range 1..10, inclusive. Unless \"outsideMode\" is true, in which case return true if the number is less or equal to 1, or greater or equal to 10.\n" +
                "\n" +
                "in1To10(5, false) → true\n" +
                "in1To10(11, false) → false\n" +
                "in1To10(11, true) → true");
        System.out.println(dojoUtil.in1To10(5, false));
        System.out.println(dojoUtil.in1To10(11, false));
        System.out.println(dojoUtil.in1To10(11, true));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("You are driving a little too fast, and a police officer stops you. Write code to compute the result, encoded as an int value: 0=no ticket, 1=small ticket, 2=big ticket. If speed is 60 or less, the result is 0. If speed is between 61 and 80 inclusive, the result is 1. If speed is 81 or more, the result is 2. Unless it is your birthday -- on that day, your speed can be 5 higher in all cases.\n" +
                "\n" +
                "caughtSpeeding(60, false) → 0\n" +
                "caughtSpeeding(65, false) → 1\n" +
                "caughtSpeeding(65, true) → 0");
        System.out.println(dojoUtil.caughtSpeeding(60, false));
        System.out.println(dojoUtil.caughtSpeeding(65, false));
        System.out.println(dojoUtil.caughtSpeeding(65, true));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("The number 6 is a truly great number. Given two int values, a and b, return true if either one is 6. Or if their sum or difference is 6. Note: the function Math.abs(num) computes the absolute value of a number.\n" +
                "love6(6, 4) → true\n" +
                "love6(4, 5) → false\n" +
                "love6(1, 5) → true");
        System.out.println(dojoUtil.love6(6,4));
        System.out.println(dojoUtil.love6(4,5));
        System.out.println(dojoUtil.love6(1,5));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("We'll say a number is special if it is a multiple of 11 or if it is one more than a multiple of 11. Return true if the given non-negative number is special. Use the % \"mod\" operator -- see Introduction to Mod\n" +
                "\n" +
                "specialEleven(22) → true\n" +
                "specialEleven(23) → true\n" +
                "specialEleven(24) → false");
        System.out.println(dojoUtil.specialEleven(22));
        System.out.println(dojoUtil.specialEleven(23));
        System.out.println(dojoUtil.specialEleven(24));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given an array length 1 or more of ints, return the difference between the largest and smallest values in the array. Note: the built-in Math.min(v1, v2) and Math.max(v1, v2) methods return the smaller or larger of two values.\n" +
                "centeredAverage([1, 2, 3, 4, 100]) → 3\n" +
                "centeredAverage([1, 1, 5, 5, 10, 8, 7]) → 5\n" +
                "centeredAverage([-10, -4, -2, -4, -2, 0]) → -3");
        int[] array27 = { 1, 2, 3, 4, 100};
        int[] array28 = { 1, 1, 5, 5, 10, 8, 7};
        int[] array29 = { -10, -4, -2, -4, -2, 0};
        System.out.println(dojoUtil.centeredAverage(array27));
        System.out.println(dojoUtil.centeredAverage(array28));
        System.out.println(dojoUtil.centeredAverage(array29));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Return the sum of the numbers in the array, returning 0 for an empty array. Except the number 13 is very unlucky, so it does not count and numbers that come immediately after a 13 also do not count.\n" +
                "sum13([1, 2, 2, 1]) → 6\n" +
                "sum13([1, 1]) → 2\n" +
                "sum13([1, 2, 2, 1, 13]) → 6");
        int[] array25 = {1, 2,2,1};
        int[] array18 = { 1,1};
        int[] array19 = { 1, 2, 2, 1, 13};
        int[] array26 = { 1, 2, 2, 1, 13,10};
        int[] array55 = { 1, 2, 13, 2, 1, 13};
        int[] array56 = { 13, 1, 2, 13, 2, 1, 13};

        System.out.println(dojoUtil.sum13(array25));
        System.out.println(dojoUtil.sum13(array18));
        System.out.println(dojoUtil.sum13(array19));
        System.out.println(dojoUtil.sum13(array26));
        System.out.println(dojoUtil.sum13(array55));
        System.out.println(dojoUtil.sum13(array56));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Return the sum of the numbers in the array, except ignore sections of numbers starting with a 6 and extending to the next 7 (every 6 will be followed by at least one 7). Return 0 for no numbers.\n" +
                "\n" +
                "sum67([1, 2, 2]) → 5\n" +
                "sum67([1, 2, 2, 6, 99, 99, 7]) → 5\n" +
                "sum67([1, 1, 6, 7, 2]) → 4");
        int[] array20 = {1, 2, 2};
        int[] array21 = {1, 2, 2, 6, 99, 99, 7};
        int[] array22 = {1, 1, 6, 7, 2};
        int[] array23 = { 6, 7};
        int[] array24 = { 6,4,5,3,7};
        int[] array60 = { 1, 6, 7, 7};
        int[] array61 = { 1, 6, 2, 6, 2, 7, 1, 6, 99, 99, 7};
        int[] array62 = { 2, 7, 6, 2, 6, 7, 2, 7};
        int[] array63 = { 6, 7, 1, 6, 7, 7};
        System.out.println(dojoUtil.sum67(array20));
        System.out.println(dojoUtil.sum67(array21));
        System.out.println(dojoUtil.sum67(array22));
        System.out.println(dojoUtil.sum67(array23));
        System.out.println(dojoUtil.sum67(array24));
        System.out.println(dojoUtil.sum67(array60));
        System.out.println(dojoUtil.sum67(array61));
        System.out.println(dojoUtil.sum67(array62));
        System.out.println(dojoUtil.sum67(array63));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Modify and return the given map as follows: if the key \"a\" has a value, set the key \"b\" to have that value, and set the key \"a\" to have the value \"\". Basically \"b\" is a bully, taking the value of \"a\".\n" +
                "\n" +
                "mapBully({\"b\": \"dirt\", \"a\": \"candy\"}) → {\"b\": \"candy\", \"a\": \"\"}\n" +
                "mapBully({\"a\": \"candy\"}) → {\"b\": \"candy\", \"a\": \"\"}\n" +
                "mapBully({\"b\": \"carrot\", \"c\": \"meh\", \"a\": \"candy\"}) → {\"b\": \"candy\", \"c\": \"meh\", \"a\": \"\"}");
        Map<String,String> map4 = new HashMap<String, String>(){{
            put("b", "dirt"); put("a", "candy");}};
        Map<String,String> map5 = new HashMap<String, String>(){{
            put("a", "candy");}};
        Map<String,String> map6 = new HashMap<String, String>(){{
            put("b", "carrot"); put("c", "meh"); put("a", "candy");}};
        System.out.println(dojoUtil.mapBully(map4));
        System.out.println(dojoUtil.mapBully(map5));
        System.out.println(dojoUtil.mapBully(map6));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Modify and return the given map as follows: if the key \"a\" has a value, set the key \"b\" to have that same value. In all cases remove the key \"c\", leaving the rest of the map unchanged.\n" +
                "\n" +
                "mapShare({\"b\": \"bbb\", \"c\": \"ccc\", \"a\": \"aaa\"}) → {\"b\": \"aaa\", \"a\": \"aaa\"}\n" +
                "mapShare({\"b\": \"xyz\", \"c\": \"ccc\"}) → {\"b\": \"xyz\"}\n" +
                "mapShare({\"d\": \"hi\", \"c\": \"meh\", \"a\": \"aaa\"}) → {\"d\": \"hi\", \"b\": \"aaa\", \"a\": \"aaa\"}");
        Map<String,String> map1 = new HashMap<String, String>(){{
            put("b", "bbb"); put("c", "ccc"); put("a", "aaa");}};
        Map<String,String> map2 = new HashMap<String, String>(){{
            put("b", "xyz"); put("c", "ccc");}};
        Map<String,String> map3 = new HashMap<String, String>(){{
            put("d", "hi"); put("c", "meh"); put("a", "aaa");}};
        System.out.println(dojoUtil.mapShare(map1));
        System.out.println(dojoUtil.mapShare(map2));
        System.out.println(dojoUtil.mapShare(map3));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Modify and return the given map as follows: for this problem the map may or may not contain the \"a\" and \"b\" keys. If both keys are present, append their 2 string values together and store the result under the key \"ab\".\n" +
                "\n" +
                "mapAB({\"b\": \"There\", \"a\": \"Hi\"}) → {\"b\": \"There\", \"a\": \"Hi\", \"ab\": \"HiThere\"}\n" +
                "mapAB({\"a\": \"Hi\"}) → {\"a\": \"Hi\"}\n" +
                "mapAB({\"b\": \"There\"}) → {\"b\": \"There\"}");
        Map<String,String> map7 = new HashMap<String, String>(){{
            put("b", "There"); put("a", "Hi");}};
        Map<String,String> map8 = new HashMap<String, String>(){{
            put("a", "Hi");}};
        Map<String,String> map9 = new HashMap<String, String>(){{
            put("b", "There");}};
        System.out.println(dojoUtil.mapAB(map7));
        System.out.println(dojoUtil.mapAB(map8));
        System.out.println(dojoUtil.mapAB(map9));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given a map of food keys and topping values, modify and return the map as follows: if the key \"ice cream\" is present, set its value to \"cherry\". In all cases, set the key \"bread\" to have the value \"butter\".\n" +
                "\n" +
                "topping1({\"ice cream\": \"peanuts\"}) → {\"bread\": \"butter\", \"ice cream\": \"cherry\"}\n" +
                "topping1({}) → {\"bread\": \"butter\"}\n" +
                "topping1({\"pancake\": \"syrup\"}) → {\"bread\": \"butter\", \"pancake\": \"syrup\"}");
        Map<String,String> map10 = new HashMap<String, String>(){{
            put("ice cream", "peanuts");}};
        Map<String,String> map11 = new HashMap<String, String>();
        Map<String,String> map12 = new HashMap<String, String>(){{
            put("pancake", "syrup");;}};
        System.out.println(dojoUtil.topping1(map10));
        System.out.println(dojoUtil.topping1(map11));
        System.out.println(dojoUtil.topping1(map12));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given a map of food keys and their topping values, modify and return the map as follows: if the key \"ice cream\" has a value, set that as the value for the key \"yogurt\" also. If the key \"spinach\" has a value, change that value to \"nuts\".\n" +
                "\n" +
                "topping2({\"ice cream\": \"cherry\"}) → {\"yogurt\": \"cherry\", \"ice cream\": \"cherry\"}\n" +
                "topping2({\"spinach\": \"dirt\", \"ice cream\": \"cherry\"}) → {\"yogurt\": \"cherry\", \"spinach\": \"nuts\", \"ice cream\": \"cherry\"}\n" +
                "topping2({\"yogurt\": \"salt\"}) → {\"yogurt\": \"salt\"}");
        Map<String,String> map13 = new HashMap<String, String>(){{
            put("ice cream", "cherry");}};
        Map<String,String> map14 = new HashMap<String, String>(){{
            put("ice cream", "cherry"); put("spinach", "dirt");}};
        Map<String,String> map15 = new HashMap<String, String>(){{
            put("yogurt", "salt");}};
        System.out.println(dojoUtil.topping2(map13));
        System.out.println(dojoUtil.topping2(map14));
        System.out.println(dojoUtil.topping2(map15));
        System.out.println("-----------------------------------------------------------------------------------------------");
        System.out.println("Given a map of food keys and topping values, modify and return the map as follows: if the key \"potato\" has a value, set that as the value for the key \"fries\". If the key \"salad\" has a value, set that as the value for the key \"spinach\"." +
                "\n" +
                "topping3({\"potato\": \"ketchup\"}) → {\"fries\": \"ketchup\", \"potato\": \"ketchup\"}\n" +
                "topping3({\"potato\": \"butter\"}) → {\"fries\": \"butter\", \"potato\": \"butter\"}\n" +
                "topping3({\"salad\": \"oil\", \"potato\": \"ketchup\"}) → {\"salad\": \"oil\", \"fries\": \"ketchup\", \"spinach\": \"oil\", \"potato\": \"ketchup\"}\n");
        Map<String,String> map16 = new HashMap<String, String>(){{
            put("potato", "ketchup");}};
        Map<String,String> map17 = new HashMap<String, String>(){{
            put("potato", "butter");}};
        Map<String,String> map18 = new HashMap<String, String>(){{
            put("salad", "oil"); put("potato", "ketchup");}};
        System.out.println(dojoUtil.topping3(map16));
        System.out.println(dojoUtil.topping3(map17));
        System.out.println(dojoUtil.topping3(map18));
        System.out.println("-----------------------------------------------------------------------------------------------");
    }
}
