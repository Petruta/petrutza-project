import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by nagype on 8/2/2016.
 */
public class DojoUtil {

    public boolean weSleep(String strDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = sdf.parse(strDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;

    }

    public boolean weSleep2(Boolean weekday, boolean vacation){
        if(!weekday || vacation){
            return true;
        }
        return false;
    }

    public boolean monkeys(boolean smileA, boolean smileB){
        return (smileA == smileB);
    }

    public int sum(int a, int b){
        if(a == b){  return 4*a;
        } else return a+b;
    }

    public int absolute(int n){
        if(n > 21)  return 2* Math.abs(n-21);
        else return Math.abs(n-21);
    }

    public boolean hour(boolean talk, int hour) {
            return (talk && (hour < 7 || hour > 20));
    }

    public boolean numbers(int a, int b){
       return (a == 10 || b==10 || (a+b == 10));

    }

    public String frontTimes(String word, int number) {
        String param="";
        if(word.length()>3){
            for (int i = 1; i <= number; i++) {
                param = param + word.substring(0, 3);
            }
        }else {
            for (int i = 1; i <= number; i++) {
                param = param + word;
            }
        }
        return param;

    }

    public String stringBits(String word){
        String word1 = "";
        for(int i=0 ; i<word.length();i+=2){
            word1 = word1 + word.substring(i,i+1);
        }
        return word1;
    }

    public int arrayCount9(int[] array){
        int count = 0;
        for(int i = 0; i < array.length; i++){
            if(array[i]==9){
                count = count+1;
            }
        }
        return count;
    }

    public boolean arrayFront9(int[] array){
        if (array.length > 4){
            for(int i=0; i < 4;i++){
                if(array[i] == 9){
                    return true;
                }
            }

        }else{
            for(int i=0; i < array.length; i++){
                if(array[i] == 9){
                    return true;
                }
            }
        }
        return false;
    }

    public String altPairs(String word) {
//        int i = 0, k = 0;
//        int[] numbers = new int[word.length()/2];
//        String word1 = "";
//        for (i = 0; i < word.length(); i += 4) {
//            numbers[k] = i;
//            if(k < numbers.length-1){
//            k=k+1;
//            }else{
//                break;
//            }
//
//            numbers[k ] = i + 1;
//            if(k < numbers.length-1){
//                k=k+1;
//            }else{
//                break;
//            }
//        }
//        word1 = word1+ numbers[0];
//        for (int l = 1; l < numbers.length; l++) {
//            if(numbers[l]!=0) {
//                word1 = word1 + word.substring(numbers[l], numbers[l] + 1);
//            }
//        }
//
//        return word1;

        String result = "";

        for (int i = 0; i < word.length(); i += 4) {
            result += word.substring(i, Math.min(word.length(), i + 2));
        }

        return result;
    }


    public String makeAbba(String word1, String word2){
        return word1+word2+word2+word1;
    }


    public String makeTags(String tag, String word){
        return "<"+tag+">"+word+"</"+tag+">";
    }

    public String putInMiddle(String word1, String word2){
        return word1.substring(0,word1.length()/2)+ word2 + word1.substring(word1.length()/2);
    }

    public String extraEnd(String word){
        if(word.length() >= 2){
            return word.substring(word.length()-2)+word.substring(word.length()-2)+word.substring(word.length()-2);
        }else{
            return "Length should be 2";
        }
    }

    public String backAround(String word){
        return word.substring(word.length()-1)  + word  + word.substring(word.length()-1);
    }

    public String front3(String words) {
        String word="";
        if(words.length()>3){
            for (int i = 1; i <= 3; i++) {
                word = word + words.substring(0, 3);
            }
        }else {
            for (int i = 1; i <= 3; i++) {
                word = word + words;
            }
        }
        return word;
    }

    public String notString(String word){
        if(word.length()<3){
            return "not"+ word;
        }else{
            if(word.substring(0,3).equals("not")){
                return word;
            }else{
                return "not"+ word;
            }
        }
    }

    public boolean posNeg(int a, int b, boolean negative) {
        if (negative && a < 0 && b < 0) {
            return true;
        }
        else if (!negative && ((a < 0 && b > 0) || (a > 0 && b < 0))) {
            return true;
        }
        return false;
    }


    public boolean or35(int number){
        if(number<0) {
            return false;
        }else {
            return ((number%3==0) || (number%5==0));
        }
    }

    public String firstHalf(String word){
        if(!(word.length()%2==0)) {
            return "Word should have even lenght";
        }
        else{
            return word.substring(0,word.length()/2);
        }
    }


    public String withoutEnd(String word){
        if(word.length()<2){
            return "Lenght should be greater than 2";
        }else{
            return word.substring(1,word.length()-1);
        }
    }

    public String nonStart(String word1, String word2){
        if(word1.length()<2 || word2.length()<2){
            return "Lenght should be greater or equal than 2";
        }else{
            return word1.substring(1)+word2.substring(1);
        }
    }

    public String front22(String word){
        if(word.length()<=2){
            return word+word+word;
        }else{
            return word.substring(0,2) + word + word.substring(0,2);
        }
    }


    public boolean startHi(String word){
        if(word.length()<2){
            return false;
        }
        return word.substring(0,2).equals("hi");
    }


    public boolean icyHot(int temp1, int temp2){
        return (temp1<0 && temp2>100) || (temp1>100 && temp2<0);
    }

    public String stringTimes(String wordS, int times){
        String word = "";
        for(int i=1;i<=times;i++){
            word = word + wordS;
        }
        return word;
    }

    public boolean firstLast6(int[] array){
        return array[0] == 6 || array[array.length-1] == 6;
    }

    public boolean sameFirstLast(int[] array){
        return array[0] == array[array.length-1];
    }

    public int sum3(int[] array) {
        int sum = 0;
        if (array.length != 3) {
            System.out.println("Array sould have lenght 3");
            return sum;
        } else {

            for (int i = 0; i < 3; i++) {
                sum = sum + array[i];
            }
        }
        return sum;
    }

    public boolean commonEnd(int[] array1, int[] array2){
        return (array1[0]  == array2[0] || array1[array1.length-1]== array2[array2.length-1]);
    }

    public int[] rotateLeft3(int[] array){
        int[] array2 = new int[array.length];
        array2[0] = array[1];
        array2[1] = array[2];
        array2[2] = array[0];
        return array2;
    }

    public int dateFashion(int you, int date){
        if((you>=8 && date>2)|| (date >=8 && you>2)) {
            return 2;
        }if (you<=2 || date <=2){
            return 0;
        } else return 1;
    }

    public int[] reverse3(int[] array){
        int[] array2 = new int[array.length];
        for(int i =0; i< array.length; i++){
            array2[i] = array[array.length-1-i];
        }
        return array2;
    }

    public int sum2(int[] array){
        if(array.length == 0){
            return 0;
        }else if(array.length == 1){
            return array[0];
        }else {
            return array[0] + array[1];
        }
    }

    public boolean cigarParty(int cigar, boolean weekend){
        if(cigar>=40 && cigar<=60 && !weekend) {
            return true;
        }
        if(cigar>=40 && weekend){
            return true;
        }else{
            return  false;
        }
    }

    public int[] maxEnd3(int[] array){
        if(array.length!=3){
            System.out.print("Array lenght should be 3");
            return array;
        }
        if(array[0] >= array[2]){

            for(int i=1;i<array.length;i++){
                array[i] = array[0];
            }
        }else{
            for(int i=0;i<array.length-1;i++){
                array[i] = array[array.length-1];
            }
        }
        return array;
    }

    public boolean squirrelPlay(int temperature, boolean isSummer){
        if(isSummer && (temperature >=60 && temperature<=100)){
            return true;
        }else if(!isSummer && (temperature >=60 && temperature<=90)){
            return true;
        }
        return false;
    }

    public int sortaSum(int a, int b){
       if( a+b>=10 && a+b<=19){ return 20;
       }else{
           return a+b;
       }
    }

    public int caughtSpeeding(int speed, boolean isBirthday) {
        if ((speed <= 60 && !isBirthday) || (speed <= 65 && isBirthday)) {
            return 0;
        } else if ((speed >= 61 && speed <= 80 && !isBirthday) || (speed >= 61 && speed <= 85 && isBirthday)) {
            return 1;
        } else return 2;
    }

    public boolean love6(int a, int b){
        return (a==6 || b==6 || (a+b)==6 || (Math.abs(a-b)==6));
    }

    public boolean specialEleven(int number){
        return (number%11==0 || number%11 ==1);
    }

    public boolean in1To10(int n, boolean outsideMode) {
        if(outsideMode){
            return n <= 1 || n >= 10;
        }
        return n >=1 && n <= 10;
    }

    public int centeredAverage(int[] array)
    {
        int max = array[0];
        int min = array[0];
        int sum = array[0];
        for(int i = 1; i < array.length; i++)
        {
            sum  = sum + array[i];
            if(array[i] > max) {
                max = array[i];
            } else if(array[i] < min) {
                min = array[i];
            }
        }
        return (sum-max-min) / (array.length - 2);
    }


    public int sum13(int[] array){
        int sum=0;
        int poz = 0;
        while(poz<array.length){
            if(array[poz]!=13){
                sum = sum + array[poz];
                poz++;
            }else{
                poz= poz+2;
            }
        }
        return sum;
    }

    public int sum67(int[] nums) {
//        int poz6 = 0;
//        int poz7 = 0;
//        int sum = 0;
//        if (array.length == 0) {
//            return sum;
//        }
//        if (array.length == 1 && (array[0] != 6 || array[0] != 7)) {
//            return array[0];
//        } else if (array.length == 1 && (array[0] == 6 || array[0] == 7)) {
//            return sum;
//        }
//        for (int i = 1; i <= array.length; i++) {
//            if (array[i-1] == 6) {
//                poz6 = i;
//            }
//            if (array[i-1] == 7) {
//                poz7 = i;
//            }
//        }
//        if (poz7 - poz6 == array.length - 1) {
//            return sum;
//        }
//
//        if (poz7 < poz6 || poz6 == 0 || poz7 == 0) {
//            for (int i = 1; i <= array.length; i++) {
//                sum += array[i-1];
//            }
//            return sum;
//        } else {
//            for (int i = 1; i <= array.length; i++) {
//                if (i < poz6 || i > poz7) {
//                    sum += array[i-1];
//                }
//            }
//            return sum;
//        }


        int sum = 0;
        boolean sixMode = false;
        for(int i = 0; i < nums.length; i++)
        {
            if(sixMode)
            {
                if(nums[i] == 7)
                    sixMode = false;
            }
            else if(nums[i] == 6)
                sixMode = true;
            else
                sum += nums[i];
        }
        return sum;
    }

    public int last2(String word){
        if(word.length()<2){
            return 0;
        }
        String substring = word.substring(word.length()-2);
        int count=0;
        for(int i=0;i<=word.length()-2;i++){
            if(substring.equals(word.substring(i, i+2))){
                count=count+1;
            }
        }
        return count-1;
    }

    public Map<String, String> mapBully(Map<String, String> map) {
        if (map.containsKey("a")) {
            map.put("b", map.get("a"));
            map.put("a", "");
        }
        return map;
    }

    public Map<String, String> mapAB(Map<String, String> map) {
        if (map.containsKey("a") && map.containsKey("b")) {
            map.put("ab", map.get("a")+map.get("b"));
        }
        return map;
    }

    public Map<String, String> mapShare(Map<String, String> map) {
        if (map.containsKey("a")) {
            map.put("b", map.get("a"));
        }
        if(map.containsKey("c")){
            map.remove("c");
        }
        return map;
    }

    public Map<String, String> topping1(Map<String, String> map) {
        if (map.containsKey("ice cream")) {
            map.put("ice cream", "cherry");
        }

        map.put("bread", "butter");
        return map;
    }

    public Map<String, String> topping2(Map<String, String> map) {
        if (map.containsKey("ice cream")) {
            map.put("yogurt", map.get("ice cream"));
        }
        if (map.containsKey("spinach")) {
            map.put("spinach", "nuts");
        }
        return map;
    }

    public Map<String, String> topping3(Map<String, String> map) {
        if (map.containsKey("potato")) {
            map.put("fries", map.get("potato"));
        }

        if (map.containsKey("salad")) {
            map.put("spinach", map.get("salad"));
        }
        return map;
    }

}
