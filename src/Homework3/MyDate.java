package Homework3;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Petruta on 8/18/2016
 */
public class MyDate {
    private int year;
    private int day;
    private int month;
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday","Thursday", "Friday", "Saturday"};
    private  int[] daysInMonth = {31, 28, 31,30,31,30,31, 31, 30,31,30,31};

    private List<Integer> month31 = Arrays.asList(1,3,5,7,8,10,12);
    private List<Integer> month30 = Arrays.asList(4,6,9,11);


    public MyDate(int year, int month, int day){
        setDate(year, month, day);
    }

    public void setDate(int year, int month, int day){
        if(isValidDate(year, month, day)){
            setYear(year);
            setMonth(month);
            setDay(day);
        } else {
            throw new IllegalArgumentException("Invalid year, month, day!");
        }

    }


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if(year >= 1 && year <= 9999) {
            this.year = year;
        }else{
            throw new IllegalArgumentException("Invalid year!");
        }
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
       if (month31.contains(getMonth()) && day >=1 && day<=31) {
           this.day = day;
       }else if(month30.contains(getMonth())&&day>=1 && day <=30){
           this.day = day;
       }else if(getMonth()==2 && isLeapYear(getYear()) && day<=29){
            this.day =day;
       }else if(getMonth()==2 && !isLeapYear(getYear()) && day<=28){
          this.day =day;
       }else {
           throw new IllegalArgumentException("Invalid day!");
       }
    }

    public boolean isValidDate(int year, int month, int day){
      if( year >= 1 && year <= 9999){
          if(month >=1 && month<=12) {
              if ((month31.contains(month) && day >= 1 && day <= 31) || (month30.contains(month) && day >= 1 && day <= 30)
                      || (month == 2 && isLeapYear(year) && day <=29) || (month == 2 && !isLeapYear(year) && day <=28)) {
                  return true;
              }
          }
      }
        return false;
    }

    public int getDayOfWeek(int year, int month, int day){
        Calendar cal = new GregorianCalendar(year, month - 1, day);  // month is 0-based
        return cal.get(Calendar.DAY_OF_WEEK)-1;
    }

    public boolean isLeapYear(int year){
        if(year%4==0 && year%100!=0){
          return true;
        }else if(year%400==0){
            return true;
        }else{
            return false;
        }

    }
    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if(month >= 1 && month <= 12){
            this.month = month;
        }else{
            throw new IllegalArgumentException("Invalid month!");
        }

    }

    public String[] getMonths() {
        return months;
    }

    public void setMonths(String[] months) {
        this.months = months;
    }

    public String[] getDays() {
        return days;
    }

    public void setDays(String[] days) {
        this.days = days;
    }

    public int[] getDaysInMonth() {
        return daysInMonth;
    }

    public void setDaysInMonth(int[] daysInMonth) {
        this.daysInMonth = daysInMonth;
    }

    public String toString(){
        return days[getDayOfWeek(getYear(), getMonth(), getDay())] + " " + getDay() + " " + months[getMonth()-1] + " "
                + getYear();
    }

    public MyDate nextDay(){
        if(getDay()<=27){
            setDay(getDay()+1);
        }else if((getDay() == 28 || getDay() == 29) && getMonth()!=2 ){
                setDay(getDay()+1);
        }else if(getDay()==28 && getMonth()==2 && isLeapYear(getYear())){
            setDay(getDay()+1);
        }else if(getDay()==29 && getMonth()==2 && isLeapYear(getYear())){
            setDay(1);
            setMonth(getMonth()+1);
        }else if(getDay()==28 && getMonth()==2 && !isLeapYear(getYear())){
            setDay(1);
            setMonth(getMonth()+1);
        }else if((getDay() == 30 && daysInMonth[getMonth()-1]==31)){
            setDay(getDay()+1);
        }else if((getDay() == 30 && daysInMonth[getMonth()-1]==30)){
            setDay(1);
            setMonth(getMonth()+1);
        }else if(getDay() == 31 && getMonth()==12){
            setDay(1);
            setMonth(1);
            setYear(getYear()+1);
        } else if(getDay() == 31 && getMonth()<12){
            setDay(1);
            setMonth(getMonth()+1);
        }
        return new MyDate(getYear(), getMonth(), getDay());
    }


    public MyDate nextYear(){
        setYear(getYear()+1);
        return new MyDate(getYear(), getMonth(), getDay());
    }

    public MyDate nextMonth(){
        if(getMonth()==12){
        setMonth(1);
        setYear(getYear()+1);
        } else{
            setMonth(getMonth()+1);
        }
        return new MyDate(getYear(), getMonth(), getDay());
    }
    public MyDate previousDay(){
        if(getDay()>1){
            setDay(getDay()-1);
        }else if((getDay() == 1  && getMonth()!=3 && getMonth()!=1)){
            setDay(daysInMonth[getMonth()]);
            setMonth(getMonth()-1);
        }else if(getDay()==1 && getMonth()==3 && isLeapYear(getYear())){
            setDay(29);
            setMonth(getMonth()-1);
        }else if(getDay()==1 && getMonth()==3 && !isLeapYear(getYear())){
            setDay(28);
            setMonth(getMonth()-1);
        }else if(getDay()==1 && getMonth()==1){
            setDay(31);
            setMonth(12);
            setYear(getYear()-1);
        }
        return new MyDate(getYear(), getMonth(), getDay());
    }


    public MyDate previousYear(){
        if(getMonth()==2 && getDay()==29 && isLeapYear(getYear()) && !isLeapYear(getYear()-1)) {
            setDay(28);
            setYear(getYear() - 1);
        }else{
            setYear(getYear() - 1);
        }
        return new MyDate(getYear(), getMonth(), getDay());
    }

    public MyDate previousMonth(){
        if(getMonth()==1){
            setMonth(12);
            setYear(getYear()-1);
        } else if(getMonth()==3 && getDay()== daysInMonth[getMonth()-1]){
            if(isLeapYear(getYear())){
            setDay(29);
            }else{
                setDay(28);
            }
            setMonth(getMonth()-1);
        }else if(getDay()== daysInMonth[getMonth()-1]){
            setDay(daysInMonth[getMonth()-2]);
            setMonth(getMonth()-1);
        }
        return new MyDate(getYear(), getMonth(), getDay());
    }
}
