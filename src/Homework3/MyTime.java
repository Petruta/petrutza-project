package Homework3;

/**
 * Created by Petruta on 8/17/2016.
 */
public class MyTime {
     private int hour;
    private int minute;
    private int second;

    public MyTime(){

    }

    public MyTime(int hour, int minute, int second){
        setHour(hour);
        setMinute(minute);
        setSecond(second);
    }

    public void setTime(int hour , int minute, int second){
        if((hour>=0 && hour<=23) && (minute>=0 && minute<=59) && (minute>=0 && minute<=59)){
            setHour(hour);
            setMinute(minute);
            setSecond(second);
        }else {
            throw new IllegalArgumentException("Invalid hour, minute, or second!");
        }

    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        if(hour>=0 && hour<=23){
            this.hour = hour;
        }else {
            throw new IllegalArgumentException("Invalid hour!");
        }

    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        if(minute>=0 && minute<=59){
            this.minute = minute;
        }else {
            throw new IllegalArgumentException("Invalid minute!");
        }
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        if(second>=0 && second<=59){
            this.second = second;
        }else {
            throw new IllegalArgumentException("Invalid second!");
        }
    }

    public String toString(){
        String hourS = "";
        String minuteS = "";
        String secondS = "";
        if (getHour()<=9){
            hourS = "0" + String.valueOf(getHour());
        } else{
            hourS = String.valueOf(getHour());
        }
        if (getMinute()<=9){
            minuteS = "0" + String.valueOf(getMinute());
        } else{
            minuteS = String.valueOf(getMinute());
        }
        if (getSecond()<=9){
            secondS = "0" + String.valueOf(getSecond());
        } else{
            secondS = String.valueOf(getSecond());
        }
        return hourS +":" + minuteS + ":" + secondS;

    }

    public MyTime nextHour(){
        if(getHour()<23) {
        setHour(getHour()+1);
        }else{
            setHour(0);
        }
        return new MyTime(getHour(),getMinute(),getSecond());
    }

    public MyTime previousHour(){
        if(getHour()==0) {
            setHour(23);
        }else{
            setHour(getHour()-1);
        }
        return new MyTime(getHour(),getMinute(),getSecond());
    }

    public MyTime nextMinute(){
        if(getMinute()<59) {
            setMinute(getMinute()+1);
        }else{
            setMinute(0);
            setHour(nextHour().getHour());
        }
        return new MyTime(getHour(),getMinute(),getSecond());
    }

    public MyTime previousMinute(){
        if(getMinute()==0) {
            setMinute(59);
            setHour(previousHour().getHour());
        }else{
            setMinute(getMinute()-1);
        }
        return new MyTime(getHour(),getMinute(),getSecond());
    }

    public MyTime nextSecond(){
        if(getSecond()<58) {
            setSecond(getSecond()+1);
        }else{
            setSecond(0);
            setMinute(nextMinute().getMinute());
        }
        return new MyTime(getHour(),getMinute(),getSecond());
    }

    public MyTime previousSecond(){
        if(getSecond()==0) {
            setSecond(59);
            setMinute(previousMinute().getMinute());
        }else{
            setSecond(getSecond()-1);
        }
        return new MyTime(getHour(),getMinute(),getSecond());
    }
}
