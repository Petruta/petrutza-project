package Homework3;

import Homework3.Author;
import Homework3.Book;

/**
 * Created by Petruta on 8/17/2016.
 */
public class TestBook {
    public static void main(String[] Args){
        // Construct an author instance
        Author ahTeck = new Author("Tan Ah Teck", "ahteck@nowhere.com", 'm');
        System.out.println(ahTeck);  // Homework3.Author's toString()

        Book dummyBook = new Book("Java for dummy", ahTeck, 19.95, 99);  // Test Homework3.Book's Constructor
        System.out.println(dummyBook);  // Test Homework3.Book's toString()

// Test Getters and Setters
        dummyBook.setPrice(29.95);
        dummyBook.setQty(28);
        System.out.println("name is: " + dummyBook.getName());
        System.out.println("price is: " + dummyBook.getPrice());
        System.out.println("qty is: " + dummyBook.getQty());
        System.out.println("Homework3.Author is: " + dummyBook.getAuthor());  // Homework3.Author's toString()
        System.out.println("Homework3.Author's name is: " + dummyBook.getAuthor().getName());
        System.out.println("Homework3.Author's email is: " + dummyBook.getAuthor().getEmail());

// Use an anonymous instance of Homework3.Author to construct a Homework3.Book instance
        Book anotherBook = new Book("more Java",
                new Author("Paul Tan", "paul@somewhere.com", 'm'), 29.95);
        System.out.println(anotherBook);  // toString()
    }
}
