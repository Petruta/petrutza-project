package Homework3;

import Homework3.MyTime;

/**
 * Created by Petruta on 8/17/2016.
 */
public class TestMyTime {
    public static void main(String[] Args) {
        MyTime a = new MyTime(10,59,34);
        System.out.println(a.toString() + " one hour later is " + (a.nextHour()).toString());
        System.out.println(a.toString() + " one minute later is " + (a.nextMinute()).toString());
        MyTime b = new MyTime(0,0,0);
        System.out.println("hour of " + b.toString() + " is " + b.getHour());
        System.out.println(b.toString() + " one second before is " + (b.previousSecond()).toString());
        MyTime c = new MyTime(23,59,59);
        System.out.println(c.toString() + " one second after is " + (b.nextSecond()).toString());

    }
}
