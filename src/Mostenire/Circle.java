package Mostenire;

/**
 * Created by nagype on 8/10/2016.
 */
public class Circle extends Shape{
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    private double  radius;

    public double getArea(){
        return 2* Math.PI * getRadius();
    }

    public double getPerimeter(){
        return Math.PI*getRadius()*getRadius();
    }
}
