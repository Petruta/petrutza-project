package Mostenire;

/**
 * Created by nagype on 8/10/2016.
 */
public class Rectangle extends Shape {

    private double width, length;

    public Rectangle (double length, double width){
        this.length = length;
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return getLength()*getWidth();
    }

    public double getPerimeter(){
        return 2*(getWidth()+getLength());
    }
}
