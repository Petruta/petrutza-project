package Mostenire;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nagype on 8/10/2016.
 */
public class ShapeApplication {
    public static void main(String[] args) {
    List<Shape> shapes = new ArrayList<>();
    Circle circle = new Circle();
    shapes.add(circle);
    Shape shape = new Shape();
    shapes.add(shape);
    Rectangle rectangle = new Rectangle(3,4);
        shape.setColor("blue");
        circle.setColor("red");
        rectangle.setColor("green");
    shapes.add(rectangle);
        for(Shape s:shapes){
            System.out.println(s.getColor());
        }

        circle.setRadius(3);
        System.out.println("perimeter " + circle.getPerimeter());
        System.out.println("Area " + circle.getArea());

        rectangle.setLength(5);
        rectangle.setWidth(3);
        System.out.println("perimeter " + rectangle.getPerimeter());
        System.out.println("Area " + rectangle.getArea());
        Square square = new Square(25);
        System.out.println(" Square " + square.getArea());
   }
}
