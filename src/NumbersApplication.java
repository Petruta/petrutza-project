/**
 * Created by nagype on 7/22/2016.
 */
public class NumbersApplication {
    public static void main(String[] args){
        NumbersUtil numbersUtil = new NumbersUtil();
        numbersUtil.printOddNumbers(11);
        System.out.println(numbersUtil.isEvenNumber(8));
        System.out.println(numbersUtil.isEvenNumber(9) );
        System.out.println(numbersUtil.countOddNumbers(30));
        System.out.println(numbersUtil.countOddNumbers(11));
    }
}
