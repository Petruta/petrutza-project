/**
 * Created by nagype on 7/22/2016.
 */
public class NumbersUtil {

    public static void printOddNumbers(int limit){
        for(int i =0; i<limit; i++){
            if(!(i%2==0)){
               System.out.print(i + " is odd number ! \n");
            }
        }
    }

    public static boolean  isEvenNumber(int number){
       return  number%2==0;
    }

    public static int countOddNumbers(int limit){
        int no = 0;
        for(int i =0; i <= limit; i++){
            if(!(i%2 == 0)){
                no++;
            }
        }
        return no;
    }
}
