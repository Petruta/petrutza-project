/**
 * Created by nagype on 7/22/2016
 */
public class Person {
    String firstName;
    String lastName;
    int age;
    String eyesColour;
    String cnp;
    Address address;

    public Person (String cnp){
        System.out.println("hvvjgjhjj");
        this.cnp = cnp;
        System.out.println("person cnp " + this.cnp);
        address = new Address();
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEyesColour() {
        return eyesColour;
    }

    public void setEyesColour(String eyesColour) {
        this.eyesColour = eyesColour;
    }

    public void printYourName(){
        System.out.println("Name : " + this.firstName + " " + this.lastName);
    }

    public void describeYourself(){
        System.out.println("Name : " + this.firstName +" "+ this.lastName +"\n" + "Age : " + this.age + "\nEyes : " + this.eyesColour);
    }

    public void birthday(int age){
        System.out.println("Happy Birthday");
        age = age+1;
        setAge(age);
    }

    public void setCity(String city){
        this.address.setCity(city);
    }

    public String getCity(){
       return  this.address.getCity();
    }

}
