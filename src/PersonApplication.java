/**
 * Created by nagype on 7/27/2016.
 */
public class PersonApplication {
    public static void main(String[] args){
        Person person = new Person("3445");
        person.setFirstName("Petruta");
        person.setLastName("Morar");
        person.setAge(20);
        person.setEyesColour("Black");
        System.out.println("Print full name");
        person.printYourName();
        System.out.println("Describe yourself");
        person.describeYourself();
        System.out.println("Happy birthday");
        person.birthday(person.getAge());
        System.out.println("Age : " + person.getAge());
        person.setCity("Cluj");
        System.out.println(person.getCity());
        System.out.println(person.toString());
    }
}
