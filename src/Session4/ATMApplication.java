package Session4;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nagype on 8/4/2016.
 */
public class ATMApplication {
    public static void main(String[] args) {
        Bancomat brd = ATMFactory.getATM("BRD");
        brd.withdrawMoney(BigDecimal.ONE);
        System.out.println(brd.balance());

        Bancomat ing = ATMFactory.getATM("ing");
        ing.withdrawMoney(BigDecimal.ONE);
        System.out.println(ing.balance());

        List<Bancomat> atms = new ArrayList<>();
        atms.add(brd);
        atms.add(ing);
        for ( Bancomat atm  :atms){
            atm.withdrawMoney( new BigDecimal(123));
        }
    }

}
