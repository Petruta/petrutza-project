package Session4;

/**
 * Created by nagype on 8/4/2016.
 */
public class ATMFactory{
    public static Bancomat getATM(String bancomat){
        if("ing".equalsIgnoreCase(bancomat)){
            return new IngATM();
        }else if ("brd".equalsIgnoreCase(bancomat)){
            return new BrdATM();
        }
        return null;
    }
}
