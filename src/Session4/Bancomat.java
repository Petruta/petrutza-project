package Session4;

import java.math.BigDecimal;

/**
 * Created by nagype on 8/4/2016.
 */
public interface Bancomat {
    public BigDecimal balance ();
    public  void withdrawMoney(BigDecimal amount);
}
