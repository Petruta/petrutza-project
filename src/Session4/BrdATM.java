package Session4;

import java.math.BigDecimal;

/**
 * Created by nagype on 8/4/2016
 */
public class BrdATM implements Bancomat {

    @Override
    public BigDecimal balance() {
        System.out.println("retrieve balance BRD");
        return BigDecimal.TEN;
    }

    @Override
    public void withdrawMoney(BigDecimal amount) {
        System.out.println("withdraw money BRD "  + amount);

    }
}
