package Session5;

/**
 * Created by nagype on 8/18/2016.
 */
public class Ball {
    protected  float x;
    protected float y;
    protected int radius;
    protected float xDelta, yDelta;

    public Ball(float x, float y, int radius, int speed, int direction){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.xDelta =  (float) (speed*Math.cos( Math.toRadians ((double) direction)));
        this.yDelta =  (float) (-speed*Math.sin( Math.toRadians ((double) direction)));
    }

    public float getXDelta() {
        return xDelta;
    }

    public void setXDelta(float xDelta) {
        this.xDelta = xDelta;
    }

    public float getYDelta() {
        return yDelta;
    }

    public void setYDelta(float yDelta) {
        this.yDelta = yDelta;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void move(){
        setX(getX()+getXDelta());
        setY(getY()+getYDelta());
    }

    public void reflectHorizontal(){
        setXDelta(-getXDelta());
    }

    public void reflectVertical(){
        setYDelta(-getYDelta());
    }

    public String toString(){
        return "Ball at ( " + getX() + "," + getY() + " of velocity ( " + getXDelta() +"," + getYDelta() + " ).";
    }

}
