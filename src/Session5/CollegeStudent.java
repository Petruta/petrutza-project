package Session5;

/**
 * Created by nagype on 8/18/2016.
 */
public class CollegeStudent extends Student {

    protected String major;
    protected int year;

    public CollegeStudent(String name, int age, String gender, String idNum, double gpa, int year, String major)  {
// use the super class’ constructor
        super(name, age, gender, idNum, gpa);

// initialize what’s new to CollegeStudent
        this.major = major;
        this.year = year;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String toString(){
        return super.toString();// + " year " + getYear() + " major " + getMajor();
    }
}
