package Session5;

/**
 * Created by nagype on 8/18/2016.
 */
public class Container {
    protected int x1;
    protected int x2;
    protected int y1;
    protected int y2;

    public Container (int x, int y, int width, int height){
        x1 = x;
        y1 = y;
        x2 = x+width-1;
        y2 = y+height-1;
    }

    public String toString(){
        return "Container at ("+ x1 + "," +y1+") to (" +x2+","+ y2+ ").";
    }

    public boolean collidesWith(Ball ball) {
        if (ball.getX() - ball.getRadius() <= this.x1 ||
                ball.getX() - ball.getRadius() >= this.x2) {
            ball.reflectHorizontal();
            return true;
        }
        if (ball.getY() - ball.getRadius() <= this.y1 ||
                ball.getY() - ball.getRadius() >= this.y2) {
            ball.reflectVertical();
            return true;
        }
        return false;
    }
}
