package Session5;

/**
 * Created by nagype on 8/18/2016.
 */
public class TestBall {
    public static void main(String[] args){

        Ball ball1 = new Ball(3.5f, 4.2f, 5, 10 ,75);
        System.out.println(ball1);
        Ball ball2 = new Ball(50f, 50f, 5, 10 ,30);
        System.out.println(ball2);

        Ball ball = new Ball(50, 50, 5, 10, 30);
        Container box = new Container(0, 0, 100, 100);
        for (int step = 0; step < 100; ++step) {
            ball.move();
            box.collidesWith(ball);
            System.out.println(ball); // manual check the position of the ball
        }
    }
}
